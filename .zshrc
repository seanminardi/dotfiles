# Path to oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
# Homebrew path
export PATH="/opt/homebrew/bin:$PATH"
# Rust
export PATH="$HOME/.cargo/bin:$PATH"
# Deno
export PATH="$DENO_INSTALL/bin:$PATH"
export PATH="$HOME/.deno/bin:$PATH"
# Ruby
export PATH="/opt/homebrew/opt/ruby/bin:$PATH"
# nvim
# export PATH="$HOME/LocalPrograms/nvim-osx64/bin:$PATH"
export PATH="$HOME/Downloads/nvim-osx64/bin:$PATH"
if type nvim > /dev/null 2>&1; then
  export VISUAL="nvim"
fi
# vscode
export PATH="/usr/local/bin:$PATH"
export PATH="/Applications/Visual Studio Code.app/Contents/Resources/app/bin:$PATH"
# dotfiles script
export PATH="$HOME/scripts:$PATH"
# libvips
export PATH="/opt/homebrew/opt/qt/bin:$PATH"
# export LDFLAGS="-L/opt/homebrew/opt/qt/lib"
# export CPPFLAGS="-I/opt/homebrew/opt/qt/include"
export LDFLAGS="-L/opt/homebrew/opt/ruby/lib"
export CPPFLAGS="-I/opt/homebrew/opt/ruby/include"
export PKG_CONFIG_PATH="/opt/homebrew/opt/qt/lib/pkgconfig"
# Flutter
export PATH="$HOME/development-tools/flutter/bin:$PATH"
# Set default editor
export EDITOR="vim"
# lvim
export PATH="$HOME/.local/bin:$PATH"
# Syntax highlighting for "less"
if type lesspipe.sh >/dev/null 2>&1; then
  LESSPIPE=`which src-hilite-lesspipe.sh`
  export LESSOPEN="| ${LESSPIPE} %s"
  export LESS=' -R '
fi


# Volta
# Maybe check if installed
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-z zsh-autosuggestions zsh-syntax-highlighting vscode)

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Aliases
source "$HOME/.zsh/.zsh_aliases"

# Functions
source "$HOME/.zsh/.zsh_functions"

export BAT_THEME="OneHalfDark"

source /Users/denniswenger/.config/broot/launcher/bash/br