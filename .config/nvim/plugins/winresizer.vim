Plug 'simeji/winresizer'

let g:winresizer_start_key = '<C-s>'
let g:winresizer_vert_resize = 5
let g:winresizer_horiz_resize = 3
