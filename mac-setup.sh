#!/bin/sh

### --- HomeBrew --- ###

# Check for Homebrew,
# Install if we don't have it
if test ! $(which brew); then
  echo "Installing homebrew"
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Update homebrew recipes
echo "Updating homebrew"
brew update

# Clis
# If you have forked this repo you might want to add or remove apps from here.
# Look at HomeBrew for the cli you want to add. This list will contain apps that NOT use the `--cask` flag.
clis=(
	bat # A better `cat` command
	neofetch # Watch system info
	ttyd
	saulpw/vd/visidata # Interactive tool for tabular data
	fd # Find files
	glow # A markdown viewer
	youtube-dl # Download youtube videos
	broot # File tree
	xsv # Working with CSV
	exa # A better `ls`
	tldr # An easier to read man page for commands and applications
	volta # A node version manager, similar to NVM
	deno # JavaScript/TypeScript runtime
	croc # Sending files
	rust # Best programming language 
	fzf # Fuzzy finding files
	httpie # Sending http requests
	jq # Working with JSON data
	imagemagick # Manipulating images
	ripgrep # New take on `grep`
	jo # Creating JSON data
	kitty # Fast terminal
)

# Apps
# If you have forked this repo you might want to add or remove apps from here.
# Look at HomeBrew for the app you want to add. This list will contain apps that use the `--cask` flag.
apps=(
	libreoffice # Open source Office. Comes with cli for converting and manipulating Office files
  firefox # Browser
  google-chrome # Browser
  spotify # Music streaming
	slack # Communication
	discord # Communication
  vlc # Media consumtion and manipulation
	visual-studio-code # Code editor
	obsidian # Graph knowledge base on markdown files
	flutter # Framework for Dart
	karabiner-elements # Working with keyboard shorcuts and events
	amethyst # Tiling window manager
	font-jetbrains-mono-nerd-font # A mono spaced font with added glyphs needed to render things in the terminal among other apps
	raycast # More feature rich and customizable alternative to Spotlight
	notion # Project and note-taking software
	mos # Control mouse scrolling. Mainly for having different settings for the trackpad and the mouse.
	1password # Password manager
	brave-browser # Browser
	postman # Client for testing http communication
)

brew tap homebrew/cask-fonts

for cli in ${clis[@]} ; do

	brew install $cli || echo "Could not install $cli. Might have been installed from other source already."

done

for app in ${apps[@]} ; do

	brew install --cask $app || echo "Could not install $app. Might have been installed from other source already."

done

brew cleanup

volta install node

### --- ZSH --- ###

if [ -d ~/.oh-my-zsh ]; then
		echo "Oh My ZSH is already installed"
	else
		echo "Installing Oh My ZSH"
		sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

echo "Downloading plugins for Zsh"

ZSH_CUSTOM=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}

# List of plugins for zsh
# The name of the plugin on the left side of the semicolon and the uri to the repo on the right
plugins=(
	'zsh-z;https://github.com/agkozak/zsh-z'
	'zsh-autosuggestions;https://github.com/zsh-users/zsh-autosuggestions'
	'zsh-syntax-highlighting;https://github.com/zsh-users/zsh-syntax-highlighting'
)

for plugin in "${plugins[@]}" ; do
	pluginArray=(${plugin//;/ })
	pluginName=${pluginArray[0]}
	pluginUri=${pluginArray[1]}

	if [ -d "$ZSH_CUSTOM/plugins/$pluginName" ]; then
		echo "$pluginName is already installed"
	else
		echo "Installing $pluginName"
		git clone $pluginUri $ZSH_CUSTOM/plugins/$pluginName
	fi
done

### --- Apple Defaults --- ###

# Add or change preferences as you see fit

# Disable OS X Gate Keeper
# Allows apps outside of the App Store to be installed
sudo spctl --master-disable
sudo defaults write /var/db/SystemPolicy-prefs.plist enabled -string no
defaults write com.apple.LaunchServices LSQuarantine -bool false

# Enable full keyboard access for all controls (e.g. enable Tab in modal dialogs)
defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

# Set the icon size of Dock items to 36 pixels for optimal size/screen-realestate
defaults write com.apple.dock tilesize -int 36

# Set the Dock to auto-hide
defaults write com.apple.dock autohide -bool true

# Group windows by application
defaults write com.apple.dock expose-group-apps -bool true

# Disable the warning when changing a file extension
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

# Finder: show all filename extensions
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# Region US, Swedish keymap
defaults write NSGlobalDomain AppleLanguages -array "en-SE" "sv-SE"
defaults write NSGlobalDomain AppleLocale -string "en_SE"

# Always show hidden files
defaults write -g AppleShowAllFiles -bool true

# Disable press and hold so that VS Code will repeat any key that is being held
defaults write com.microsoft.VSCode ApplePressAndHoldEnabled -bool false

# Disable automatically rearrange Spaces based on recent use
defaults write com.apple.dock mru-spaces -bool false

# Enable automatically hiding the menu bar
defaults write .GlobalPreferences _HIHideMenuBar -bool true

# Do not show siri in menu bar
defaults write com.apple.Siri StatusMenuVisible -bool false

# Do not show spotlight in menu bar
defaults write ~/Library/Preferences/ByHost/com.apple.Spotlight MenuItemHidden -bool true

# Start week on monday
defaults write .GlobalPreferences AppleFirstWeekday -dict gregorian 2

# Show battery percentage in menu bar
defaults write ~/Library/Preferences/ByHost/com.apple.controlcenter.plist BatteryShowPercentage -bool true

# Display time with seconds
defaults write com.apple.menuextra.clock ShowSeconds -bool true

# Show input menu in menu bar
defaults write com.apple.TextInputMenu visible -bool false

# Remove items from trash after 30 days
defaults write com.apple.finder FXRemoveOldTrashItems -bool true

# Show path in Finder
defaults write com.apple.finder ShowPathbar -bool true

# Remove all apps from Dock (Simply re-add the ones you actually want)
defaults write com.apple.dock persistent-apps -array

# Where to save screenshots. Mac default is the desktop
ScreenshotsDirectory="${HOME}/Pictures/Screenshots"
[[ ! -d "$ScreenshotsDirectory" ]] && mkdir -p "dirname $ScreenshotsDirectory" || echo "Folder for screenshots has already been created"
defaults write com.apple.screencapture location -string "$ScreenshotsDirectory"

killall Dock
killall Finder